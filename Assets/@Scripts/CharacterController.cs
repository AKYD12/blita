﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour {

    public int charSelected;
    public GameObject[] selectedChar;


    public void CharacterSwap()
    {
        if (charSelected <= 0)
        {
            charSelected = 1;
        }
        else
        {
            charSelected = 0;
        }
    }

}

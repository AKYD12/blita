﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[System.Serializable]
public class Buildings
{
    public string Class;
    public string Name;
    public int[] Damage;
    public int[] Level;
    public int[] Cost;
}

[System.Serializable]
public class BuildingsList
{
    public List<Buildings> Buildings = new List<Buildings>();
}

[System.Serializable]
public class LevelContent
{
    public int ID;
    public string TileSet;
    public int CharPos0X;
    public int CharPos0Y;
    public int CharPos1X;
    public int CharPos1Y;

}

[System.Serializable]
public class LevelList
{
    public List<LevelContent> LevelContent = new List<LevelContent>();
}

public class JsonDataBase : MonoBehaviour {
    
    //public BuildingsList buildingList = new BuildingsList();
    public LevelList levelList = new LevelList();

    
    public string jsonString;


	// Use this for initialization
	public void Start () {
        TextAsset asset = Resources.Load(jsonString) as TextAsset;

        
        //buildingList = JsonUtility.FromJson<BuildingsList>(asset.text);
        levelList = JsonUtility.FromJson<LevelList>(asset.text);

        //foreach (LevelContent lC in levelList.LevelContent)
        //{
        //    //Debug.Log(lC.ID);
        //    //Debug.Log(lC.TileSet);
        //    //Debug.Log(lC.CharPos0X);
        //    //Debug.Log(lC.CharPos0Y);
        //    //Debug.Log(lC.CharPos1X);
        //    //Debug.Log(lC.CharPos1Y);
        //}
        /*
        foreach (Units unit in unitsList.Units)
        {
            Debug.Log(unit.Class);
            Debug.Log(unit.Name);
            Debug.Log(unit.Health);
        }
        
        foreach (Buildings building in buildingList.Buildings)
        {
            Debug.Log(building.Class);
            Debug.Log(building.Name);
            Debug.Log(building.Level);
        }*/
    }
}

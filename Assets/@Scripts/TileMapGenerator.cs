﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelMap
{
    public string name;

    public int mapSizeX;
    public int mapSizeY;
    public string levelMapData;
}


public class TileMapGenerator : MonoBehaviour {

    private void Awake()
    {
        Instance = this;
    }

    public static TileMapGenerator Instance;

    public CharacterController characterController;
    public JsonDataBase jsonDataBase;

    public int levelSelected;

    public LevelMap[] levelMap;
    public TileType[] tileType;

    public int[,] tileCoordinate;
    public Node[,] graph;

    public int[] tileTypeData;

    //public string[] mapData;
    public string[] newMapdata;


    //public string[] ReadLevelData()
    //{
    //    TextAsset levelList = Resources.Load("Level" + levelSelected) as TextAsset;
    //    string data = levelList.text.Replace(Environment.NewLine, string.Empty);
    //    return data.Split('-');
    //}

    
    // Use this for initialization
    void Start () {
        int _charPos0X = jsonDataBase.levelList.LevelContent[levelSelected].CharPos0X;
        int _charPos0Y = jsonDataBase.levelList.LevelContent[levelSelected].CharPos0Y;
        int _charPos1X = jsonDataBase.levelList.LevelContent[levelSelected].CharPos1X;
        int _charPos1Y = jsonDataBase.levelList.LevelContent[levelSelected].CharPos1Y;
        GeneratePathfindingGraph();
        GenerateMapData();
        GenerateUnitPosition(_charPos0X, _charPos0Y, _charPos1X, _charPos1Y);
    }


    void GenerateUnitPosition(int charPox0x, int charPos0y, int charPos1x, int charPos1y)
    {
        for (int a = 0; a < characterController.selectedChar.Length; a++)
        {
            Units _charStartPos = characterController.selectedChar[a].GetComponent<Units>();
            _charStartPos.tileX = charPox0x;
            _charStartPos.tileY = charPos0y;
        }
    }

    void GenerateMapData()
    {
        //Try TO JSON
        //mapData = ReadLevelData();

        //mapData = jsonDataBase.levelList.LevelContent[levelSelected].TileSet.ToCharArray();

        //int mapX = mapData[0].ToCharArray().Length;
        //int mapY = mapData.Length;

        newMapdata = new string[jsonDataBase.levelList.LevelContent[levelSelected].TileSet.Length];
        for (int i = 0; i < newMapdata.Length; i++)
        {
            newMapdata[i] = System.Convert.ToString(jsonDataBase.levelList.LevelContent[levelSelected].TileSet[i]);
        }

        tileTypeData = new int[newMapdata.Length];
        for (int a = 0; a < newMapdata.Length; a++)
        {
            tileTypeData[a] = Int32.Parse(newMapdata[a]);
        }

        //tileTypeData = new int[mapData.Length];
        //for (int a = 0; a < mapData.Length; a++)
        //{
        //    tileTypeData[a] = Int32.Parse(mapData[a]);
        //}
        

        tileCoordinate = new int[levelMap[levelSelected].mapSizeX, levelMap[levelSelected].mapSizeY];


        int _indexTile=0;
        for (int x = 0; x < levelMap[levelSelected].mapSizeX; x++)
        {
            for (int y = 0; y < levelMap[levelSelected].mapSizeY; y++)
            {
                tileCoordinate[x, y] = tileTypeData[_indexTile];

                //Debug.Log( "Coordinate" + tileCoordinate[x,y]);
                _indexTile++;
            }
        }

        // Allocate our map tiles
        //tileCoordinate = new int[mapSizeX, mapSizeY];

        //int x, y;

        //for (x = 0; x < mapSizeX; x++)
        //{
        //    for (y = 0; y < mapSizeX; y++)
        //    {
        //        tileCoordinate[x, y] = 0;
        //    }
        //}

        //// Initialize our map tiles to be grass
        //for (x = 0; x < mapSizeX; x++)
        //{
        //    for (y = 0; y < mapSizeX; y++)
        //    {
        //        tileCoordinate[x, y] = 0;
        //    }
        //}

        //// Make a big swamp area
        //for (x = 3; x <= 5; x++)
        //{
        //    for (y = 0; y < 4; y++)
        //    {
        //        tileCoordinate[x, y] = 1;
        //    }
        //}

        //// Let's make a u-shaped mountain range
        //tileCoordinate[4, 4] = 2;
        //tileCoordinate[5, 4] = 2;
        //tileCoordinate[6, 4] = 2;
        //tileCoordinate[7, 4] = 2;
        //tileCoordinate[8, 4] = 2;

        //tileCoordinate[4, 5] = 2;
        //tileCoordinate[4, 6] = 2;
        //tileCoordinate[8, 5] = 2;
        //tileCoordinate[8, 6] = 2;

        GenerateMapVisual();
    }



    private void GenerateMapVisual()
    {
        for (int x = 0; x < levelMap[levelSelected].mapSizeX; x++)
        {
            for (int y = 0; y < levelMap[levelSelected].mapSizeY; y++)
            {
                TileType tt = tileType[tileCoordinate[x, y]];

                GameObject go = (GameObject)Instantiate(tt.tileVisualPrefab
                    , new Vector3(x, y, 0), Quaternion.identity);

                ClickAbleTiles ct = go.GetComponent<ClickAbleTiles>();
                ct.tileX = x;
                ct.tileY = y;
                ct.map = this;
            }
        }
    }



    void GeneratePathfindingGraph()
    {
        // Initialize the array
        graph = new Node[levelMap[levelSelected].mapSizeX, levelMap[levelSelected].mapSizeY];

        // Initialize a Node for each spot in the array
        for (int x = 0; x < levelMap[levelSelected].mapSizeX; x++)
        {
            for (int y = 0; y < levelMap[levelSelected].mapSizeY; y++)
            {
                graph[x, y] = new Node();
                graph[x, y].x = x;
                graph[x, y].y = y;
            }
        }

        // Now that all the nodes exist, calculate their neighbours
        for (int x = 0; x < levelMap[levelSelected].mapSizeX; x++)
        {
            for (int y = 0; y < levelMap[levelSelected].mapSizeY; y++)
            {

                // This is the 4-way connection version:
                /*				if(x > 0)
                                    graph[x,y].neighbours.Add( graph[x-1, y] );
                                if(x < mapSizeX-1)
                                    graph[x,y].neighbours.Add( graph[x+1, y] );
                                if(y > 0)
                                    graph[x,y].neighbours.Add( graph[x, y-1] );
                                if(y < mapSizeY-1)
                                    graph[x,y].neighbours.Add( graph[x, y+1] );
                */

                // This is the 8-way connection version (allows diagonal movement)
                // Try left
                if (x > 0)
                {
                    graph[x, y].neighbours.Add(graph[x - 1, y]);
                    if (y > 0)
                        graph[x, y].neighbours.Add(graph[x - 1, y - 1]);
                    if (y < levelMap[levelSelected].mapSizeY - 1)
                        graph[x, y].neighbours.Add(graph[x - 1, y + 1]);
                }

                // Try Right
                if (x < levelMap[levelSelected].mapSizeX - 1)
                {
                    graph[x, y].neighbours.Add(graph[x + 1, y]);
                    if (y > 0)
                        graph[x, y].neighbours.Add(graph[x + 1, y - 1]);
                    if (y < levelMap[levelSelected].mapSizeY - 1)
                        graph[x, y].neighbours.Add(graph[x + 1, y + 1]);
                }

                // Try straight up and down
                if (y > 0)
                    graph[x, y].neighbours.Add(graph[x, y - 1]);
                if (y < levelMap[levelSelected].mapSizeY - 1)
                    graph[x, y].neighbours.Add(graph[x, y + 1]);

                // This also works with 6-way hexes and n-way variable areas (like EU4)
            }
        }
    }

    public Vector3 TileCoordToWorldCoord(int x, int y)
    {
        return new Vector3(x, y, 0);
    }

    public bool UnitCanEnterTile(int x, int y)
    {

        // We could test the unit's walk/hover/fly type against various
        // terrain flags here to see if they are allowed to enter the tile.

        return tileType[tileCoordinate[x, y]].isWalkable;
    }

    public float CostToEnterTile(int sourceX, int sourceY, int targetX, int targetY)
    {

        TileType tt = tileType[tileCoordinate[targetX, targetY]];

        if (UnitCanEnterTile(targetX, targetY) == false)
            return Mathf.Infinity;

        float cost = tt.movementCost;

        if (sourceX != targetX && sourceY != targetY)
        {
            // We are moving diagonally!  Fudge the cost for tie-breaking
            // Purely a cosmetic thing!
            cost += 0.001f;
        }

        return cost;

    }

    public void GeneratePathTo(int x, int y)
    {
        // Clear out our unit's old path.
        Units _charToGO = characterController.selectedChar[characterController.charSelected].GetComponent<Units>();
        _charToGO.currentPath = null;

        if (UnitCanEnterTile(x, y) == false)
        {
            // We probably clicked on a mountain or something, so just quit out.
            return;
        }

        Dictionary<Node, float> dist = new Dictionary<Node, float>();
        Dictionary<Node, Node> prev = new Dictionary<Node, Node>();

        // Setup the "Q" -- the list of nodes we haven't checked yet.
        List<Node> unvisited = new List<Node>();

        Node source = graph[
                            _charToGO.tileX,
                            _charToGO.tileY
                            ];

        Node target = graph[
                            x,
                            y
                            ];

        dist[source] = 0;
        prev[source] = null;

        // Initialize everything to have INFINITY distance, since
        // we don't know any better right now. Also, it's possible
        // that some nodes CAN'T be reached from the source,
        // which would make INFINITY a reasonable value
        foreach (Node v in graph)
        {
            if (v != source)
            {
                dist[v] = Mathf.Infinity;
                prev[v] = null;
            }

            unvisited.Add(v);
        }

        while (unvisited.Count > 0)
        {
            // "u" is going to be the unvisited node with the smallest distance.
            Node u = null;

            foreach (Node possibleU in unvisited)
            {
                if (u == null || dist[possibleU] < dist[u])
                {
                    u = possibleU;
                }
            }

            if (u == target)
            {
                break;  // Exit the while loop!
            }

            unvisited.Remove(u);

            foreach (Node v in u.neighbours)
            {
                //float alt = dist[u] + u.DistanceTo(v);
                float alt = dist[u] + CostToEnterTile(u.x, u.y, v.x, v.y);
                if (alt < dist[v])
                {
                    dist[v] = alt;
                    prev[v] = u;
                }
            }
        }

        // If we get there, the either we found the shortest route
        // to our target, or there is no route at ALL to our target.

        if (prev[target] == null)
        {
            // No route between our target and the source
            return;
        }

        List<Node> currentPath = new List<Node>();

        Node curr = target;

        // Step through the "prev" chain and add it to our path
        while (curr != null)
        {
            currentPath.Add(curr);
            curr = prev[curr];
        }

        // Right now, currentPath describes a route from out target to our source
        // So we need to invert it!

        currentPath.Reverse();

        _charToGO.currentPath = currentPath;
    }

}

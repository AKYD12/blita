﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickAbleTiles : MonoBehaviour {
    public int tileX;
    public int tileY;
    public TileMapGenerator map;

    void OnMouseUp()
    {
        Debug.Log("Click!");

        if (EventSystem.current.IsPointerOverGameObject())
            return;

        map.GeneratePathTo(tileX, tileY);
    }
}
